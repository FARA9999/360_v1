<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('hello', function (Request $request){
    echo 'Hello first route';
});

Route::group(['namespace'=>'API'],function () {
    Route::post('register', 'UserController@register');
    Route::post('login', 'UserController@login');

        
    Route::group(['middleware' => ['auth:api']],function () {
        Route::post('profile-update', 'UserController@profileUpdt');
        Route::post('review/add', 'ReviewController@add');
        Route::get('review/show/{item}', 'ReviewController@show');
        Route::post('review/edit/{item}', 'ReviewController@edit');
        Route::delete('review/delete/{item}', 'ReviewController@delete');

        
        Route::post('review/find-by-phone', 'ReviewController@findByPhone');
        Route::get('review/all', 'ReviewController@all');

        
        Route::get('review/all-request', 'ReviewController@allRequest');

        
        Route::get('user', 'UserController@getUser');
        Route::get('logout', 'UserController@getLogout');
    });
    
});