<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Http\Model\Entity\Contacts;
use App\Http\Model\Entity\Review;

use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    protected $table = 'users';
    protected $fillable = ['id', 'login', 'email', 'password', 'type_id', 'name', 'phone', 'remember_token', 'created_at', 'updated_at', 'deleted_at', 'total_token', 'used_token'];

    protected $hidden = [
        'password', 'remember_token'
    ];

    function reviewUser(){

        return $this->hasMany('App\Http\Model\Entity\Review');

    }

    function contactUser(){

        return $this->hasMany('App\Http\Model\Entity\Contact');

    }
}
