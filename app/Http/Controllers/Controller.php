<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function sendApiTrueResponse($message, $data = false){
        $res = [];
        $res['success'] = true;
        $res['message'] = $message;
        
        if ($data)
            $res['data'] = $data;

        return response()->json($res);
    }

    function sendApiFalseResponse($message, $data = false){
        $res = [];
        $res['success'] = false;
        $res['message'] = $message;

        if ($data)
            $res['data'] = $data;

        return response()->json($res);
    }
}
