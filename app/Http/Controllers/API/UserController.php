<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use App\User;
use Hash;
use Auth;

class UserController extends Controller {

    public function register(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'max:255',
            'email' => 'max:255|unique:users,email',
            'login' => 'max:255|unique:users,login',
            'password' => 'max:11',
        ]);

        if($validator->fails())
            return response()->json([
                'success' => false,
                'message' => $validator->messages()
            ]);
        
        $user = User::create([
            'login' => $request->get('login'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')) ,
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'total_token' => 0,
            'used_token' => 0
        ]);

        return response()->json([
            'success' => true,
            'user' => $user,
            'token' => $user->createToken('MyApp')->accessToken
        ]);
    }


    public function login(Request $request){
        if (!Auth::attempt(['login' => $request->input('login'), 'password' => $request->input('password')]) 
                && !Auth::attempt(['phone' => $request->input('phone'), 'password' => $request->input('password')])) 
            return response()->json([
                'success' => false,
                'message' => 'Wrong credentials'
            ]);
        
        $user = Auth::user();

        return response()->json([
            'success' => true,
            'user' => $user,
            'token' => $user->createToken('MyApp')->accessToken
        ]);
    }

    public function profileUpdt(Request $request, User $user){
        $user = $request->user();

        if ($request->email)
            $user->email = $request->email;

        if ($request->name)
            $user->name = $request->name;

        if ($request->phone)
            $user->phone = $request->phone;
        
        $user->save();
      
        
        return response()->json([
            'success' => true,
            'user' => $user,
            'token' => $user->createToken('MyApp')->accessToken
        ]); 
    }

    public function getUser(Request $request){
        $user = Auth::user();

        return response()->json([
            'success' => true,
            'user' => $user
        ]);
    }

    public function getLogout(Request $request) {
        $user = Auth::user()->tokens()->delete();

        return response()->json([
            'success' => true
        ]);
    }


}

