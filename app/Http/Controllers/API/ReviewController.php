<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;
use App\Http\Model\Entity\Review;
use App\Http\Model\Entity\UserRequest;


class ReviewController extends Controller {
    public function add (Request $request){

        if(Review::where('user_id', $request->user()->id)->where('phone', $request->phone)->count()) 
            return $this->sendApiFalseResponse('review already exist');
                
        $review = new Review();
        $review->user_id = $request->user()->id;
        $review->name = $request->name;
        $review->good_review = $request->good_review;
        $review->bad_review = $request->bad_review;
        $review->phone = $request->phone;
        $review->comment = $request->bad_review;
        $review->save();

        $request->user()->total_token = $request->user()->total_token +1;
        $request->user()->save();
       
        return $this->sendApiTrueResponse('Review has been created', $review);
    }

    public function show(Request $request, Review $item){
        if($item->user_id != $request->user()->id)
            return $this->sendApiFalseResponse('Current review does not belong to User');

        
        return $this->sendApiTrueResponse('Element founded', $item);
    }

    public function edit (Request $request,  Review $item){ 
        if($item->user_id != $request->user()->id)
            return $this->sendApiFalseResponse('Current review does not belong to User');

        if($request->phone && Review::where('user_id', $request->user()->id)->where('phone', $request->phone)->where('id', '<>', $item->id)->count()) 
            return $this->sendApiFalseResponse('review already exist');

        $item->name = $request->name;
        $item->good_review = $request->good_review;
        $item->bad_review = $request->bad_review;
        $item->comment = $request->comment;
        if ($request->phone)
            $item->phone = $request->phone;
        $item->save();


        
        return $this->sendApiTrueResponse('Review has been updated', $item);
    }

    public function delete(Request $request, Review $item){
        if($item->user_id != $request->user()->id)
            return $this->sendApiFalseResponse('Current review does not belong to User');

        $item->delete();
        
        $request->user()->total_token = $request->user()->total_token - 1;
        $request->user()->save();
        
        return $this->sendApiTrueResponse('Review has been deleted');
    }

    public function findByPhone(Request $request){// из рекувеста получаем сотовый ишем отзывы на сотовый и возврашаем. также делаем пометку в моделе юзер реквест
        $user_request = UserRequest::where('user_id', $request->user()->id)->where('phone', $request->phone)->count();

        if (!$user_request && $request->user()->total_token < Review::COST_REVIEW)
            return $this->sendApiFalseResponse('You need has '.(!$request->user()->total_token ? 0 : $request->user()->total_token).' reviews. You need add '.Review::COST_REVIEW.' reviews');

        if (!$user_request && Review::where('phone', $request->phone)->count() == 0)
            return $this->sendApiTrueResponse('We note have any review on this phone');
        
        if (!$user_request){
            $request->user()->total_token = $request->user()->total_token - Review::COST_REVIEW;
            $request->user()->used_token = $request->user()->used_token + Review::COST_REVIEW;
            $request->user()->save();

            UserRequest::create(['user_id' => $request->user()->id, 'phone'=> $request->phone]);
        }

        $items = Review::where('phone', $request->phone)->get();

        return $this->sendApiTrueResponse('Reviews founded', $items);     
    }

    function all(Request $request){
        $user_id = $request->user()->id;
        $items = Review::where('user_id', $user_id)->get();

        return $this->sendApiTrueResponse('Reviews founded', $items);   
    }

    function allRequest(Request $request){
        $user_request = UserRequest::where('user_id', $request->user()->id)->pluck('phone')->toArray();

        
        return $this->sendApiTrueResponse('All requests', $user_request);   
    }
}
