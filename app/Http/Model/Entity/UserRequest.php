<?php
namespace App\Http\Model\Entity;

use Illuminate\Database\Eloquent\Model;

class UserRequest extends Model {
    protected $table = 'user_request';
    protected $fillable = [ 'id', 'user_id', 'phone', 'second_phone', 'created_at', 'updated_at'];

    public function reviewUser (){
        return $this->belongTo('App\User', 'user_id');
    }
}
