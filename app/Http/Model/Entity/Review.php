<?php
namespace App\Http\Model\Entity;

use Illuminate\Database\Eloquent\Model;

class Review extends Model {

    protected $table = 'review';
    protected $fillable = [ 'id', 'user_id', 'phone', 'good_review', 'bad_review', 'comment', 'name', 'created_at', 'updated_at', 'deleted_at'];

    CONST COST_REVIEW = 3;

    public function reviewUser (){
        
        return $this->belongTo('App\User', 'user_id');
    }
}
