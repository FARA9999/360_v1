<?php

namespace App\Http\Model\Entity;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model {

    protected $table = 'contacts';
    protected $fillable = [ 'id', 'user_id', 'phone', 'created_at', 'updated_at', 'deleted_at'];

    public function contactUser (){
        
        return $this->belongTo('App\User', 'user_id');
    }
}
